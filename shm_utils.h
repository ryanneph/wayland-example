/* file contents copied for use under public domain or CC0 from:
 * https://wayland-book.com/surfaces/shared-memory.html
 */
#ifndef __SHM_UTILS_H__
#define __SHM_UTILS_H__

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif

#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>

static inline void
randname(char *buf)
{
   struct timespec ts;
   clock_gettime(CLOCK_REALTIME, &ts);
   long r = ts.tv_nsec;
   for (int i = 0; i < 6; ++i) {
      buf[i] = 'A'+(r&15)+(r&16)*2;
      r >>= 5;
   }
}

static inline int
create_shm_file(void)
{
   int retries = 100;
   do {
      char name[] = "/wl_shm-XXXXXX";
      randname(name + sizeof(name) - 7);
      --retries;
      int fd = shm_open(name, O_RDWR | O_CREAT | O_EXCL, 0600);
      if (fd >= 0) {
         shm_unlink(name);
         return fd;
      }
   } while (retries > 0 && errno == EEXIST);
   return -1;
}

static inline int
allocate_shm_file(size_t size)
{
   int fd = create_shm_file();
   if (fd < 0)
      return -1;
   int ret;
   do {
      ret = ftruncate(fd, size);
   } while (ret < 0 && errno == EINTR);
   if (ret < 0) {
      close(fd);
      return -1;
   }
   return fd;
}

#endif // __SHM_UTILS_H__
