#ifndef __DRM_UTILS_H__
#define __DRM_UTILS_H__

#define DRM_FORMAT_MOD_VENDOR_NONE    0
#define DRM_FORMAT_MOD_VENDOR_INTEL   0x01
#define DRM_FORMAT_MOD_VENDOR_AMD     0x02

#define DRM_FORMAT_RESERVED  ((1ULL << 56) - 1)

#define fourcc_mod_get_vendor(modifier) \
   (((modifier) >> 56) & 0xff)

#define fourcc_mod_is_vendor(modifier, vendor) \
   (fourcc_mod_get_vendor(modifier) == DRM_FORMAT_MOD_VENDOR_## vendor)

#define fourcc_mod_code(vendor, val) \
   ((((uint64_t)DRM_FORMAT_MOD_VENDOR_## vendor) << 56) | ((val) & 0x00ffffffffffffffULL))

#define DRM_FORMAT_MOD_LINEAR   0
#define DRM_FORMAT_MOD_INVALID  fourcc_mod_code(NONE, DRM_FORMAT_RESERVED)

#endif // __DRM_UTILS_H__
