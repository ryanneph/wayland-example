#ifndef __LINUX_DMABUF_H__
#define __LINUX_DMABUF_H__

#include "protocols/linux-dmabuf-unstable-v1.h"

struct Gbm_Buffer;

struct Linux_Dmabuf {
   zwp_linux_dmabuf_v1 *instance;
   uint32_t version;
};

Linux_Dmabuf *
linux_dmabuf_create(uint32_t version);

void
linux_dmabuf_destroy(Linux_Dmabuf *linux_dmabuf);

void
get_default_dmabuf_feedback(Linux_Dmabuf *linux_dmabuf);

void
get_surface_dmabuf_feedback(Linux_Dmabuf *linux_dmabuf, struct wl_surface *surface);

struct wl_buffer *
linux_dmabuf_create_wl_buffer_from_gbm_buffer(Gbm_Buffer *gbm_buffer);

#endif // __LINUX_DMABUF_H__
