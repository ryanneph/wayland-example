#ifndef __GBM_UTILS_H__
#define __GBM_UTILS_H__

#include <gbm.h>

struct Gbm_Buffer {
   gbm_device *device;
   const char *device_name;

   gbm_bo *bo;
   int dmabuf_fd;
   void *mapping;
   void *map_data;

   uint32_t format;
   uint64_t modifier;
   uint32_t width, height, stride;
   uint32_t bytes_per_pixel;
   uint32_t usage;
};

Gbm_Buffer *
gbm_buffer_create(uint32_t width, uint32_t height, uint32_t format, uint64_t modifier,
      const char *rendernode_filename = NULL);

void
gbm_buffer_destroy(Gbm_Buffer *gbm_buffer);

void *
gbm_buffer_map_writeonly(Gbm_Buffer *buffer, uint32_t *out_stride, void **out_map_cookie);

void
gbm_buffer_unmap(Gbm_Buffer *buffer, void *map_cookie);

#endif // __GBM_UTILS_H__
