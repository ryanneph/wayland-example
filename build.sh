for file in protocols/*.xml; do
  wayland-scanner client-header < "${file}" > "${file%%.xml}.h"
  wayland-scanner private-code < "${file}" > "${file%%.xml}.c"
done

c++ -g -O0 -Wall -Wno-attributes \
  -o main main.cpp \
  -lwayland-client \
  -lgbm
