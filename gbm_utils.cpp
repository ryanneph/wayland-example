#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include "drm_utils.h"
#include "gbm_utils.h"
#include "utils.h"

Gbm_Buffer *
gbm_buffer_create(uint32_t width, uint32_t height, uint32_t format, uint64_t modifier,
      const char *rendernode_filename)
{
   // TODO(ryan): scan the rendernode files from D128+ to find one that suits the needs.
   // libdrm has some utils for this...
   if (!rendernode_filename)
      rendernode_filename = "/dev/dri/renderD128";

   int dev_fd = open(rendernode_filename, O_RDWR);
   if (dev_fd < 0) {
      ERROR("Failed to open rendernode file '%s'", rendernode_filename);
      return NULL;
   }

   struct gbm_device *gbm = gbm_create_device(dev_fd);
   close(dev_fd);
   if (!gbm) {
      ERROR("Failed to open gbm device for node '%s'", rendernode_filename);
      return NULL;
   }
   const char *backend_name = gbm_device_get_backend_name(gbm);

   const uint32_t usage = GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING;
#if USE_GBM_MODIFIERS
   gbm_bo *bo = gbm_bo_create_with_modifiers2(gbm, width, height, format, &modifier,
         1, usage);
#else
   gbm_bo *bo = gbm_bo_create(gbm, width, height, format, usage | GBM_BO_USE_LINEAR);
   if (bo) {
      modifier = gbm_bo_get_modifier(bo);
      assert(modifier == DRM_FORMAT_MOD_LINEAR || modifier == DRM_FORMAT_MOD_INVALID); // implicit modifier
   }
#endif
   if (!bo) {
      ERROR("Failed to create gbm_bo from device '%s'", rendernode_filename);
      gbm_device_destroy(gbm);
      return NULL;
   }

   uint32_t bits_per_pixel = gbm_bo_get_bpp(bo);
   assert(bits_per_pixel % 8 == 0);
   uint32_t stride = gbm_bo_get_stride(bo);

   int bo_fd = gbm_bo_get_fd(bo);
   if (bo_fd < 0) {
      ERROR("Failed to get dmabuf fd from gbm_bo.");
      gbm_bo_destroy(bo);
      gbm_device_destroy(gbm);
      return NULL;
   }

   DEBUG("Created gbm buffer (backend=%s, width=%u, height=%u, stride=%u)",
        backend_name, width, height, stride);

   Gbm_Buffer *gbm_buffer = (Gbm_Buffer *)malloc(sizeof(*gbm_buffer));
   *gbm_buffer = {
      .device = gbm,
      .device_name = backend_name,
      .bo = bo,
      .dmabuf_fd = bo_fd,
      .format = format,
      .modifier = modifier,
      .width = width,
      .height = height,
      .stride = stride,
      .bytes_per_pixel = bits_per_pixel / 8,
      .usage = usage,
   };
   return gbm_buffer;
}

void
gbm_buffer_destroy(Gbm_Buffer *gbm_buffer) {
   close(gbm_buffer->dmabuf_fd);
   gbm_bo_destroy(gbm_buffer->bo);
   gbm_device_destroy(gbm_buffer->device);
   free(gbm_buffer);
}

void *
gbm_buffer_map_writeonly(Gbm_Buffer *buffer, uint32_t *out_stride, void **out_map_cookie) {
   *out_map_cookie = NULL; // gbm requires this to be initialized
   void *mapping = gbm_bo_map(buffer->bo, 0, 0, buffer->width, buffer->height,
                              GBM_BO_TRANSFER_WRITE, out_stride, out_map_cookie);
   return mapping;
}

void
gbm_buffer_unmap(Gbm_Buffer *buffer, void *map_cookie) {
   assert(map_cookie);
   gbm_bo_unmap(buffer->bo, map_cookie);
}
