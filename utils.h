#ifndef __UTILS_H__
#define __UTILS_H__

#include <assert.h>
#include <math.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <wayland-client.h>

// Options
#define LINUX_DMABUF_VERSION_TO_BIND 4
#define STRESS_TEST_COUNT 0
#define DRAW_FRAME_COUNT -1 // -1 is INFINITE
#define USE_GBM_BUFFERS 1
#define USE_GBM_MODIFIERS 0
#define VERBOSE 1

struct Wayland_Global {
   uint32_t name;
   uint32_t version;
};
struct Global_State {
   struct wl_display *display;
   struct wl_registry *registry;
   struct wl_compositor *compositor;
   struct xdg_wm_base *xdg_wm;
   struct wl_shm *shm;
   struct Linux_Dmabuf *linux_dmabuf;

   Wayland_Global compositor_global;
   Wayland_Global shm_global;
   Wayland_Global linux_dmabuf_global;
   Wayland_Global xdg_wm_base_global;

   uint32_t first_draw_time_ms;
   uint32_t last_draw_time_ms;
};
extern Global_State state;

#define ARRAY_SIZE(x)  sizeof(x) / sizeof(*(x))
#define MIN(x, y)  ((x) <= (y) ? (x) : (y))

#ifdef UNUSED
#elif defined(__GNUC__)
# define UNUSED __attribute__((unused))
#else
# define UNUSED
#endif

#ifdef UNREACHABLE
#elif defined(__GNU_C__)
#define UNREACHABLE(format, ...) \
   do { \
      __builtin_unreachable(); \
      ERROR(format, ##__VA_ARGS__); \
      abort(); \
   } while (0);

#else
#define UNREACHABLE(format, ...) \
   do { \
      ERROR(format, ##__VA_ARGS__); \
      abort(); \
   } while (0);
#endif

static inline void
log_to_stream(FILE *stream, const char *format, ...) {
   va_list vargs;
   va_start(vargs, format);
   vfprintf(stdout, format, vargs);
   va_end(vargs);
}
#define ERROR(format, ...)  log_to_stream(stderr, format "\n", ##__VA_ARGS__)
#define INFO(format, ...)   log_to_stream(stdout, format "\n", ##__VA_ARGS__)

#if VERBOSE
#define DEBUG(format, ...)   log_to_stream(stdout, format "\n", ##__VA_ARGS__)
#else
#define DEBUG(format, ...)
#endif

static inline size_t
sprintf_if_space(char *to, int32_t remain_count, const char *format, ...) {
   va_list vargs;
   va_start(vargs, format);

   size_t format_len = vsnprintf(NULL, 0, format, vargs);
   va_start(vargs, format);
   vsnprintf(to, remain_count, format, vargs);

   va_end(vargs);
   int32_t count = MIN((int32_t)format_len, remain_count);
   assert(count >= 0);
   return count;
}

static inline size_t
strcpy_if_space(char *to, const char *from, int32_t remain_count) {
   strncpy(to, from, remain_count);
   int32_t count = MIN((int32_t)strlen(to), remain_count);
   assert(count >= 0);
   return count;
}

#define wl_array_for_each_typed(type, pos, array) \
   for (pos = (type *)(array)->data; \
         (const char *) pos < ((const char *) (array)->data + (array)->size); \
         (pos)++)

#endif // __UTILS_H__
