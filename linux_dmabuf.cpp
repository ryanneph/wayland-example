#include <errno.h>
#include <sys/mman.h>

#include "drm_utils.h"
#include "gbm_utils.h"
#include "linux_dmabuf.h"
#include "utils.h"

// single-unit build
#include "protocols/linux-dmabuf-unstable-v1.c"


static char format_string_buf[255];
UNUSED static char *
describe_drm_format(uint32_t format) {
   size_t count = 0;

   format_string_buf[count++] = (char)( format        & 0xFF);
   format_string_buf[count++] = (char)((format >>  8) & 0xFF);
   format_string_buf[count++] = (char)((format >> 16) & 0xFF);
   format_string_buf[count++] = (char)((format >> 24) & 0xFF);

   format_string_buf[count] = 0;
   return format_string_buf;
}

static char modifier_string_buf[255];
UNUSED static char *
describe_drm_modifier(uint64_t mod) {
   const uint64_t mod_val = mod & 0x00FFFFFFFFFFFFFFULL;
   size_t count = 0;
#define BUF_NEXT      (&modifier_string_buf[count])
#define REMAIN_COUNT  ((int32_t)ARRAY_SIZE(modifier_string_buf) - count)

   if (mod == DRM_FORMAT_MOD_LINEAR) {
      count += strcpy_if_space(BUF_NEXT, "LINEAR", REMAIN_COUNT);
   } else if (mod == DRM_FORMAT_MOD_INVALID) {
      count += strcpy_if_space(BUF_NEXT, "INVALID", REMAIN_COUNT);
   } else {
      const char *vendor;
      const char *mod_string = NULL;
      if (fourcc_mod_is_vendor(mod, AMD)) {
         vendor = "AMD";
      } else if (fourcc_mod_is_vendor(mod, INTEL)) {
         vendor = "INTEL";
         if      (mod_val ==  1)  mod_string = "X_TILED";
         else if (mod_val ==  2)  mod_string = "Yf_TILED";
         else if (mod_val ==  3)  mod_string = "Y_TILED";
         else if (mod_val ==  4)  mod_string = "Y_TILED_CCS";
         else if (mod_val ==  5)  mod_string = "Yf_TILED_CCS";
         else if (mod_val ==  6)  mod_string = "Y_TILED_GEN12_RC_CCS";
         else if (mod_val ==  7)  mod_string = "Y_TILED_GEN12_MC_CCS";
         else if (mod_val ==  8)  mod_string = "Y_TILED_GEN12_RC_CCS_CC";
         else if (mod_val ==  9)  mod_string = "4_TILED";
      } else if (fourcc_mod_is_vendor(mod, NONE)) {
         vendor = "";
      } else {
         vendor = "UNKOWN";
      }

      if (strlen(vendor))
         count += sprintf_if_space(BUF_NEXT, REMAIN_COUNT, "%s_", vendor);

      if (mod_string) {
         count += strcpy_if_space(BUF_NEXT, mod_string, REMAIN_COUNT);
      } else {
         // just format the 56-bit value as a number string
         count += sprintf_if_space(BUF_NEXT, REMAIN_COUNT, "%lu", mod_val);
      }
   }

   assert(REMAIN_COUNT >= 0);
   modifier_string_buf[count] = 0;
   return modifier_string_buf;

#undef BUF_NEXT
#undef REMAIN_COUNT
}


//
// interface: zwp_linux_dmabuf_v1 (version 3)
//
static void
linux_dmabuf_cb_format(void *data, struct zwp_linux_dmabuf_v1 *zwp_linux_dmabuf_v1,
      uint32_t format)
{
#if VERBOSE
   const char *format_string = describe_drm_format(format);
   DEBUG("FORMAT: format='%s'    [f=0x%x]", format_string, format);
#endif
}

static void
linux_dmabuf_cb_modifier(void *data, struct zwp_linux_dmabuf_v1 *zwp_linux_dmabuf_v1, uint32_t format,
      uint32_t modifier_hi, uint32_t modifier_lo)
{
#if VERBOSE
   const uint64_t modifier = ((uint64_t)modifier_hi << 32) | modifier_lo;
   const char *format_string = describe_drm_format(format);
   const char *mod_string = describe_drm_modifier(modifier);
   DEBUG("FORMAT: format='%s'  modifier='%s'    [f=0x%x m=(hi=0x%x, lo=0x%x)]",
         format_string, mod_string, format, modifier_hi, modifier_lo);
#endif
}

static zwp_linux_dmabuf_v1_listener linux_dmabuf_listener = {
   .format = linux_dmabuf_cb_format,
   .modifier = linux_dmabuf_cb_modifier,
};


//
// interface: zwp_linux_dmabuf_feedback_v1 (version 4)
//
struct Format {
   uint32_t format;
   uint32_t __pad;
   uint64_t modifier;
};

struct Format_Table {
   Format *formats;
   uint32_t map_size;
   uint32_t count;
};

// TODO(ryanneph): make this a per-instance storage
struct {
   dev_t main_device;
   Format_Table format_table;
} dmabuf_feedback_state;

static void
linux_dmabuf_feedback_cb_done(void *data, struct zwp_linux_dmabuf_feedback_v1 *feedback)
{
   DEBUG("Feedback done");
   assert(dmabuf_feedback_state.format_table.formats);
   int ret = munmap(dmabuf_feedback_state.format_table.formats, dmabuf_feedback_state.format_table.map_size);
   if (ret == -1) {
      ERROR("Failed to unmap dmabuf format table (error %d: %s)", errno, strerror(errno));
   }

   dmabuf_feedback_state = {};
}

static void
linux_dmabuf_feedback_cb_format_table(void *data, struct zwp_linux_dmabuf_feedback_v1 *feedback,
      int32_t fd, uint32_t size)
{
   DEBUG("Format table fd=%d size=%u", fd, size);

   void *map_addr = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
   if (map_addr == MAP_FAILED) {
      ERROR("Failed to map dmabuf format table (error %d: %s)", errno, strerror(errno));
      return;
   }

   dmabuf_feedback_state.format_table = {
      .formats = (Format *)map_addr,
      .map_size = size,
      .count = size / (uint32_t)sizeof(Format),
   };
}

static void
linux_dmabuf_feedback_cb_main_device(void *data, struct zwp_linux_dmabuf_feedback_v1 *feedback,
      struct wl_array *device)
{
   assert(device->size <= sizeof(uint64_t));
   memcpy(&dmabuf_feedback_state.main_device, device->data, device->size);
   DEBUG("main device=0x%x", dmabuf_feedback_state.main_device);
}

static void
linux_dmabuf_feedback_cb_tranche_done(void *data, struct zwp_linux_dmabuf_feedback_v1 *feedback)
{
   DEBUG("tranche done");
}

static void
linux_dmabuf_feedback_cb_tranche_target_device(void *data, struct zwp_linux_dmabuf_feedback_v1 *feedback,
      struct wl_array *device)
{
   DEBUG("tranche target device=0x%x", *((uint64_t *)device->data));
}

static void
linux_dmabuf_feedback_cb_tranche_formats(void *data, struct zwp_linux_dmabuf_feedback_v1 *feedback,
      struct wl_array *indices)
{
   DEBUG("tranche formats indices=(data=%p size=%zu)", indices->data, indices->size);
   assert(dmabuf_feedback_state.format_table.formats);

#if VERBOSE
   size_t count = indices->size / 2;
   for (uint32_t i = 0; i < count; i++) {
      const uint16_t index = ((uint16_t *)indices->data)[i];
      const Format *row = &dmabuf_feedback_state.format_table.formats[index];

      const char *format_string = describe_drm_format(row->format);
      const char *mod_string = describe_drm_modifier(row->modifier);
      DEBUG("  [%d]: FORMAT: format='%s'  modifier='%s'    [f=0x%x m=(hi=0x%x, lo=0x%x)]",
            index, format_string, mod_string, row->format, row->modifier >>32,
            row->modifier && 0xFFFFFFFF);
   }
#endif
}

static void
linux_dmabuf_feedback_cb_tranche_flags(void *data, struct zwp_linux_dmabuf_feedback_v1 *feedback,
      uint32_t flags)
{
   DEBUG("tranche flags=%u", flags);
}

static zwp_linux_dmabuf_feedback_v1_listener linux_dmabuf_feedback_listener = {
   .done                  = linux_dmabuf_feedback_cb_done,
   .format_table          = linux_dmabuf_feedback_cb_format_table,
   .main_device           = linux_dmabuf_feedback_cb_main_device,
   .tranche_done          = linux_dmabuf_feedback_cb_tranche_done,
   .tranche_target_device = linux_dmabuf_feedback_cb_tranche_target_device,
   .tranche_formats       = linux_dmabuf_feedback_cb_tranche_formats,
   .tranche_flags         = linux_dmabuf_feedback_cb_tranche_flags,
};


//
// interface: zwp_linux_buffer_params_v1
//
void
linux_buffer_params_cb_created(void *data,
       struct zwp_linux_buffer_params_v1 *zwp_linux_buffer_params_v1,
       struct wl_buffer *buffer)
{
   INFO("wl buffer created from dmabuf");
}

void
linux_buffer_params_cb_failed(void *data,
      struct zwp_linux_buffer_params_v1 *zwp_linux_buffer_params_v1)
{
   ERROR("Failed to create wl_buffer from dmabuf");
}

static zwp_linux_buffer_params_v1_listener linux_buffer_params_listener = {
   .created = linux_buffer_params_cb_created,
   .failed  = linux_buffer_params_cb_failed,
};


//
// External API
//
void
get_default_dmabuf_feedback(Linux_Dmabuf *linux_dmabuf) {
   auto *feedback = zwp_linux_dmabuf_v1_get_default_feedback(linux_dmabuf->instance);
   zwp_linux_dmabuf_feedback_v1_add_listener(feedback, &linux_dmabuf_feedback_listener, NULL);

   wl_display_roundtrip(state.display);
   zwp_linux_dmabuf_feedback_v1_destroy(feedback);
}

void
get_surface_dmabuf_feedback(Linux_Dmabuf *linux_dmabuf, struct wl_surface *surface) {
   auto *feedback = zwp_linux_dmabuf_v1_get_surface_feedback(linux_dmabuf->instance, surface);
   zwp_linux_dmabuf_feedback_v1_add_listener(feedback, &linux_dmabuf_feedback_listener, NULL);

   wl_display_roundtrip(state.display);
   zwp_linux_dmabuf_feedback_v1_destroy(feedback);
}

Linux_Dmabuf *
linux_dmabuf_create(uint32_t version) {
   if (!state.linux_dmabuf_global.name) {
      ERROR("Missing support for zwp_linux_dmabuf_v1.");
      return NULL;
   }
   assert(version <= state.linux_dmabuf_global.version);

   Linux_Dmabuf *linux_dmabuf = (Linux_Dmabuf *)malloc(sizeof(*linux_dmabuf));
   linux_dmabuf->version = version;
   linux_dmabuf->instance =
      (zwp_linux_dmabuf_v1 *)wl_registry_bind(state.registry, state.linux_dmabuf_global.name,
            &zwp_linux_dmabuf_v1_interface, version);

   DEBUG("Bound to zwp_linux_dmabuf_v1 with handle=%p (version: supported=%u bound=%u)",
         linux_dmabuf->instance, state.linux_dmabuf_global.version, version);

   if (version <= 3) {
      int error = zwp_linux_dmabuf_v1_add_listener(linux_dmabuf->instance, &linux_dmabuf_listener, NULL);
      assert(!error);

      DEBUG("Registered zwp_linux_dmabuf_v1_listener");
   }

   return linux_dmabuf;
}

void
linux_dmabuf_destroy(Linux_Dmabuf *linux_dmabuf) {
   INFO("Destroying zwp_linux_dmabuf_v1 instance with handle=%p", linux_dmabuf->instance);
   zwp_linux_dmabuf_v1_destroy(linux_dmabuf->instance);
   free(linux_dmabuf);
}

struct wl_buffer *
linux_dmabuf_create_wl_buffer_from_gbm_buffer(Linux_Dmabuf *linux_dmabuf, Gbm_Buffer *gbm_buffer) {
   struct zwp_linux_buffer_params_v1 *params = zwp_linux_dmabuf_v1_create_params(linux_dmabuf->instance);
   zwp_linux_buffer_params_v1_add_listener(params, &linux_buffer_params_listener, NULL);

   zwp_linux_buffer_params_v1_add(params, gbm_buffer->dmabuf_fd, 0, 0, gbm_buffer->stride,
         (gbm_buffer->modifier >> 32) & 0xFFFFFFFF, gbm_buffer->modifier & 0xFFFFFFFF);

   struct wl_buffer *wl_buffer = zwp_linux_buffer_params_v1_create_immed(params, gbm_buffer->width,
         gbm_buffer->height, gbm_buffer->format, 0);
   INFO("Created wl_buffer %p from dmabuf", wl_buffer);

   zwp_linux_buffer_params_v1_destroy(params);
   return wl_buffer;
}
