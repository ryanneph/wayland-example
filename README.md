# Wayland-Example

A simple example of a client that directly writes window surface contents to mapped GBM or Shmem buffers on
each frame.

Build: `./build.sh`  
Run: `./main`

Configurable options are provided as `#define`s in `utils.h`.
* Shared buffer type:
  * Shmem buffers: `#define USE_GBM_BUFFERS 0`
  * GBM buffers: `#define USE_GBM_BUFFERS 1`
