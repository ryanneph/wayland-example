#include "gbm_utils.h"
#include "linux_dmabuf.h"
#include "shm_utils.h"
#include "utils.h"
#include "protocols/xdg-shell.h"

// single-unit build
#include "gbm_utils.cpp"
#include "linux_dmabuf.cpp"
#include "protocols/xdg-shell.c"

struct Global_State state = {};

struct Swapchain;

enum Buffer_Type {
   BUFFER_TYPE_SHM = 0,
   BUFFER_TYPE_GBM = 1,
};
struct Shm_Pool {
   struct wl_shm_pool *instance;
   int fd;
   uint32_t size;
   void *mapping;

   uint32_t refcount;
};
struct Swapchain_Buffer {
   Buffer_Type type;
   uint32_t index;
   struct wl_buffer *buffer;

   uint32_t bytes_per_pixel;

   Swapchain *swapchain;

   // linked-list
   Swapchain_Buffer *prev, *next;
};
struct Swapchain_Buffer_Shm {
   Swapchain_Buffer base;

   Shm_Pool *pool;  // refcounted
   uint32_t offset; // to first pixel from pool mapped address
   uint32_t stride; // bytes i.e. `align32(width * bytes_per_pixel, alignment)`
};
struct Swapchain_Buffer_Gbm {
   Swapchain_Buffer base;

   Gbm_Buffer *gbm_buffer;
};
struct Swapchain {
   uint32_t count;
   uint32_t format;   // defined per the Buffer_Type
   uint64_t modifier; // defined per the Buffer_Type
   uint32_t width;
   uint32_t height;

   // buffers released by the server are stored here
   Swapchain_Buffer *ready_buffers;

   Swapchain_Buffer *buffers[];
};
struct Surface_State {
   uint32_t width, height;
};
inline bool
operator== (const Surface_State &s1, const Surface_State &s2) {
   return !memcmp(&s1, &s2, sizeof(s1));
}
struct Surface {
   struct wl_surface   *instance;
   struct xdg_surface  *xdg_surface;
   struct xdg_toplevel *xdg_toplevel;

   Surface_State state;
   Surface_State pending;

   Swapchain *swapchain;

   uint32_t draw_pending_count;
};

static Shm_Pool *
shm_pool_ref(Shm_Pool *pool) {
   pool->refcount += 1;
   return pool;
}

static bool
shm_pool_unref(Shm_Pool *pool) {
   assert(pool->refcount > 0);
   if (pool->refcount > 1) {
      pool->refcount -= 1;
      return false;
   }

   int ret;
   ret = munmap(pool->mapping, pool->size);
   ret |= close(pool->fd);
   assert(!ret);

   wl_shm_pool_destroy(pool->instance);
   return true;
}

static void *
swapchain_buffer_map_writeonly(Swapchain_Buffer *swapchain_buffer, uint32_t *out_stride, void **out_map_cookie) {
   switch (swapchain_buffer->type) {
      case BUFFER_TYPE_SHM: {
         // persistent mapping
         Swapchain_Buffer_Shm *shm_buffer = (Swapchain_Buffer_Shm *)swapchain_buffer;
         *out_stride = shm_buffer->stride;
         *out_map_cookie = NULL;
         return ((uint8_t *)shm_buffer->pool->mapping) + shm_buffer->offset;
      }

      case BUFFER_TYPE_GBM: {
         Gbm_Buffer *gbm_buffer = ((Swapchain_Buffer_Gbm *)swapchain_buffer)->gbm_buffer;
         void *mapping = gbm_buffer_map_writeonly(gbm_buffer, out_stride, out_map_cookie);
         if (!mapping)
            ERROR("Failed to map GBM buffer");
         return mapping;
      }

      default:
         UNREACHABLE("Unhandled swapchain buffer type %d in %s", __func__);
   }
}

static void
swapchain_buffer_unmap(Swapchain_Buffer *swapchain_buffer, void *map_cookie) {
   switch (swapchain_buffer->type) {
      case BUFFER_TYPE_SHM:
         // persistent mapping - nothing to do
         return;

      case BUFFER_TYPE_GBM: {
         Gbm_Buffer *gbm_buffer = ((Swapchain_Buffer_Gbm *)swapchain_buffer)->gbm_buffer;
         gbm_buffer_unmap(gbm_buffer, map_cookie);
         return;
      }

      default:
         UNREACHABLE("Unhandled swapchain buffer type %d in %s", __func__);
   }
}


//
// interface: global registry
//
void
registry_cb_global(void *data, struct wl_registry *registry, uint32_t name,
      const char *interface, uint32_t version)
{
   if (!strcmp(interface, wl_compositor_interface.name)) {
      state.compositor_global = {name, version};
   } else if (!strcmp(interface, wl_shm_interface.name)) {
      state.shm_global = {name, version};
   } else if (!strcmp(interface, xdg_wm_base_interface.name)) {
      state.xdg_wm_base_global = {name, version};
   } else if (!strcmp(interface, zwp_linux_dmabuf_v1_interface.name) && version >= 3) {
      state.linux_dmabuf_global = {name, version};
   } else {
      DEBUG("ignoring registry global %u on interface '%s' (version %u)", name, interface, version);
      return;
   }
}

void
registry_cb_global_remove(void *data, struct wl_registry *registry, uint32_t name) {
   DEBUG("registry global %u removed", name);
}

static const wl_registry_listener registry_listener = {
   .global = registry_cb_global,
   .global_remove = registry_cb_global_remove,
};


//
// interface: wl_compositor
//
static void
wl_buffer_listener_cb_release(void *data, struct wl_buffer *wl_buffer) {
   Swapchain_Buffer *buffer = (Swapchain_Buffer *)data;
   Swapchain *swapchain = buffer->swapchain;

   if (!swapchain->ready_buffers) {
      // linked-list init
      swapchain->ready_buffers = buffer;
      buffer->next = buffer->prev = NULL;
   } else {
      // linked-list prepend
      buffer->prev = NULL;
      buffer->next = swapchain->ready_buffers;
      swapchain->ready_buffers->prev = buffer;
      swapchain->ready_buffers = buffer;
   }
}

struct wl_buffer_listener wl_buffer_listener = {
   .release = wl_buffer_listener_cb_release,
};

static void
bind_compositor() {
   assert(state.compositor_global.name);

   state.compositor = (struct wl_compositor *)wl_registry_bind(
         state.registry, state.compositor_global.name, &wl_compositor_interface,
         state.compositor_global.version);
   DEBUG("Bound to wl_compositor (version=%u)", state.compositor_global.version);
}

//
// interface: wl_shm
//
static void
bind_shm() {
   assert(state.shm_global.name);

   state.shm = (struct wl_shm *)wl_registry_bind(
         state.registry, state.shm_global.name, &wl_shm_interface, state.shm_global.version);
   DEBUG("Bound to wl_shm (version=%u)", state.shm_global.version);
}


//
// interface: xdg_wm_base
//
static void
xdg_wm_cb_ping(void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial) {
   xdg_wm_base_pong(state.xdg_wm, serial);
}

static xdg_wm_base_listener xdg_wm_listener = {
   .ping = xdg_wm_cb_ping,
};

static void
bind_xdg_shell() {
   assert(state.xdg_wm_base_global.name);

   state.xdg_wm = (struct xdg_wm_base *)wl_registry_bind(
         state.registry, state.xdg_wm_base_global.name, &xdg_wm_base_interface,
         state.xdg_wm_base_global.version);
   DEBUG("Bound to xdg_wm_base (version=%u)", state.xdg_wm_base_global.version);

   xdg_wm_base_add_listener(state.xdg_wm, &xdg_wm_listener, NULL);
   DEBUG("Registered xdg_wm_base_listener");
}


//
// Buffer management
//
static Shm_Pool *
create_shm_pool(uint32_t buffer_size, uint32_t buffer_count) {
   const uint32_t shm_size = buffer_count * buffer_size;

   int shm_fd = allocate_shm_file(shm_size);
   if (shm_fd < 0) {
      ERROR("Failed to allocate shm");
      abort();
   }

   void *mapping = mmap(NULL, shm_size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
   if (!mapping) {
      ERROR("Failed to map shm with error %d: %s", errno, strerror(errno));
      abort();
   }

   Shm_Pool *shm_pool = (Shm_Pool *)malloc(sizeof(*shm_pool));
   *shm_pool = {
      .instance = wl_shm_create_pool(state.shm, shm_fd, shm_size),
      .fd = shm_fd,
      .size = shm_size,
      .mapping = mapping,
   };
   DEBUG("Created wl_shm_pool (handle=%p, fd=%d, size=%u, mapping=%p)",
         shm_pool->instance, shm_pool->fd, shm_pool->size, shm_pool->mapping);

   return shm_pool;
}

static Swapchain *
create_swapchain(uint32_t width, uint32_t height, uint32_t format, uint64_t modifier,
      Buffer_Type buffer_type, uint32_t buffer_count)
{
   DEBUG("Creating swapchain (width=%u, height=%u, format=%u, modifier=%lu, count=%u)",
         width, height, format, modifier, buffer_count);

   // a simple packed allocation
   Swapchain *swapchain = (Swapchain *)malloc(sizeof(Swapchain) + buffer_count * sizeof(Swapchain_Buffer *));
   *swapchain = {
      .count = buffer_count,
      .format = format,
      .modifier = modifier,
      .width = width,
      .height = height,
   };

   if (buffer_type == BUFFER_TYPE_SHM) {
      assert(format == WL_SHM_FORMAT_ARGB8888 || format == WL_SHM_FORMAT_XRGB8888);
      const uint32_t bytes_per_pixel = 4;
      const uint32_t stride = bytes_per_pixel * width; // TODO(ryan): alignment requirements?
      const uint32_t buffer_size = stride * height;

      Shm_Pool *shm_pool = create_shm_pool(buffer_size, buffer_count);
      assert(shm_pool);

      for (uint32_t i = 0; i < buffer_count; i++) {
         const uint32_t offset = i * buffer_size;
         struct wl_buffer *wl_buffer =
            wl_shm_pool_create_buffer(shm_pool->instance, offset, width, height, stride, format);

         Swapchain_Buffer_Shm *buffer = (Swapchain_Buffer_Shm *)malloc(sizeof(*buffer));
         *buffer = {
            .base = {
               .type = BUFFER_TYPE_SHM,
               .index = i,
               .buffer = wl_buffer,
               .bytes_per_pixel = bytes_per_pixel,
               .swapchain = swapchain,
            },
            .pool = shm_pool_ref(shm_pool),
            .offset = offset,
            .stride = stride,
         };
         swapchain->buffers[i] = (Swapchain_Buffer *)buffer;

         wl_buffer_add_listener(wl_buffer, &wl_buffer_listener, (void *)swapchain->buffers[i]);
      }
   } else if (buffer_type == BUFFER_TYPE_GBM) {
      for (uint32_t i = 0; i < buffer_count; i++) {
         // TODO(ryanneph): use queried surface feedback to send a supported format+modifier,
         // size, and usage
         // get_default_dmabuf_feedback(state.linux_dmabuf);

         Gbm_Buffer *gbm_buffer = gbm_buffer_create(width, height, format, modifier);
         assert(gbm_buffer);

         struct wl_buffer *wl_buffer =
            linux_dmabuf_create_wl_buffer_from_gbm_buffer(state.linux_dmabuf, gbm_buffer);

         Swapchain_Buffer_Gbm *buffer = (Swapchain_Buffer_Gbm *)malloc(sizeof(*buffer));
         *buffer = {
            .base = {
               .type = BUFFER_TYPE_GBM,
               .index = i,
               .buffer = wl_buffer,
               .bytes_per_pixel = gbm_buffer->bytes_per_pixel,
               .swapchain = swapchain,
            },
            .gbm_buffer = gbm_buffer,
         };
         swapchain->buffers[i] = (Swapchain_Buffer *)buffer;

         wl_buffer_add_listener(wl_buffer, &wl_buffer_listener, (void *)swapchain->buffers[i]);
      }
   } else {
      ERROR("Unknown buffer type %d", buffer_type);
      assert(false);
   }

   // linked-list insert back-to-front
   assert(buffer_count >= 1);
   swapchain->ready_buffers = swapchain->buffers[buffer_count - 1];
   for (int32_t i = buffer_count-2; i >= 0; i--) {
      Swapchain_Buffer *buffer = swapchain->buffers[i];
      swapchain->ready_buffers->prev = buffer;
      buffer->next = swapchain->ready_buffers;
      swapchain->ready_buffers = buffer;
   }

   return swapchain;
}

static void
destroy_swapchain_buffer(Swapchain_Buffer *buffer) {
   if (buffer->type == BUFFER_TYPE_SHM) {
      Swapchain_Buffer_Shm *shm_buffer = (Swapchain_Buffer_Shm *)buffer;
      shm_pool_unref(shm_buffer->pool);
   } else if (buffer->type == BUFFER_TYPE_GBM) {
      Swapchain_Buffer_Gbm *gbm_buffer = (Swapchain_Buffer_Gbm *)buffer;
      gbm_buffer_destroy(gbm_buffer->gbm_buffer);
   } else {
      ERROR("Unknown buffer type %d", buffer->type);
      assert(false);
   }

   wl_buffer_destroy(buffer->buffer);
}

static void
destroy_swapchain(Swapchain *swapchain) {
   DEBUG("Destroying swapchain %p", swapchain);

   for (uint32_t i = 0; i < swapchain->count; i++)
      destroy_swapchain_buffer(swapchain->buffers[i]);

   free(swapchain);
}


//
// Surface Management
//
static void
toplevel_cb_configure(void *data, struct xdg_toplevel *xdg_toplevel, int32_t width, int32_t height,
      struct wl_array *states)
{
   Surface *surface = (Surface *)data;

   if (width || height) {
      DEBUG("EVENT: toplevel::configure(width=%d height=%d)", width, height);
      assert(width && height);
      surface->pending.width = width;
      surface->pending.height = height;
   }

#if 0
   enum xdg_toplevel_state *state;
   wl_array_for_each_typed(enum xdg_toplevel_state, state, states) {
      INFO("State is %d", *state);
   }
#endif
}

static xdg_toplevel_listener toplevel_listener = {
   .configure = toplevel_cb_configure,
};

static void
xdg_surface_cb_configure(void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
   Surface *surface = (Surface *)data;

   DEBUG("EVENT: xdg_surface::configure");
   if (surface->state == surface->pending)
      return;

   DEBUG("committing surface state: (width=%u height=%u) to (width=%u height=%u)",
         surface->state.width, surface->state.height, surface->pending.width,
         surface->pending.height);

   surface->state = surface->pending;

   // TODO(ryan): skip when possible (state change, but not resizing)
   if (surface->swapchain)
      destroy_swapchain(surface->swapchain);
#if USE_GBM_BUFFERS
   surface->swapchain = create_swapchain(surface->state.width, surface->state.height,
         GBM_FORMAT_XRGB8888, DRM_FORMAT_MOD_LINEAR, BUFFER_TYPE_GBM, 2);
#else
   surface->swapchain = create_swapchain(surface->state.width, surface->state.height,
         WL_SHM_FORMAT_XRGB8888, 0 /* ignored */, BUFFER_TYPE_SHM, 2);
#endif

   xdg_surface_set_window_geometry(surface->xdg_surface, 0, 0, surface->state.width,
         surface->state.height);
   xdg_surface_ack_configure(surface->xdg_surface, serial);

   surface->draw_pending_count += 1;
}

static xdg_surface_listener xdg_surface_listener = {
   .configure = xdg_surface_cb_configure,
};

static void
surface_frame_cb_done(void *data, struct wl_callback *callback, uint32_t time_ms);

static wl_callback_listener surface_frame_listener = {
   .done = surface_frame_cb_done,
};

static void
surface_frame_cb_done(void *data, struct wl_callback *callback, uint32_t time_ms) {
   Surface *surface = (Surface *)data;

   wl_callback_destroy(callback);
   surface->draw_pending_count += 1;

   state.last_draw_time_ms = time_ms;
   if (!state.first_draw_time_ms)
      state.first_draw_time_ms = time_ms;
}

static Surface *
create_surface(const char *title, uint32_t width, uint32_t height) {
   struct wl_surface *wl_surface = wl_compositor_create_surface(state.compositor);
   struct xdg_surface *xdg_surface = xdg_wm_base_get_xdg_surface(state.xdg_wm, wl_surface);
   struct xdg_toplevel *xdg_toplevel = xdg_surface_get_toplevel(xdg_surface);

   Surface *surface = (Surface *)malloc(sizeof(Surface));
   *surface = {
      .instance = wl_surface,
      .xdg_surface = xdg_surface,
      .xdg_toplevel = xdg_toplevel,
      .pending = {
         .width = width,
         .height = height,
      },
   };

   xdg_surface_add_listener(xdg_surface, &xdg_surface_listener, (void *)surface);
   xdg_toplevel_add_listener(xdg_toplevel, &toplevel_listener, (void *)surface);

   // configure and commit
   xdg_toplevel_set_title(surface->xdg_toplevel, title);
   wl_surface_commit(surface->instance);

   DEBUG("Created surface %p", surface->instance);
   return surface;
}

UNUSED static void
destroy_surface(Surface *surface) {
   xdg_toplevel_destroy(surface->xdg_toplevel);
   xdg_surface_destroy(surface->xdg_surface);
   wl_surface_destroy(surface->instance);
   free(surface);
}


//
// Program
//
static void
draw_to_buffer(Swapchain_Buffer *buffer) {
   DEBUG("Draw frame (buffer_index=%u)", buffer->index);
   const Swapchain *swapchain = buffer->swapchain;

   const uint32_t width = swapchain->width;
   const uint32_t height = swapchain->height;
   const uint32_t bytes_per_pixel = buffer->bytes_per_pixel;

   uint32_t stride;
   void *map_cookie;
   uint8_t *data = (uint8_t *)swapchain_buffer_map_writeonly(buffer, &stride, &map_cookie);
   if (!data) {
      ERROR("Failed to map swapchain buffer");
      return;
   }

   struct Pixel {
      uint8_t b, g, r, a;
   };
   assert(sizeof(Pixel) == 4);

   /* Draw moving checkerboard */
   const float min_checker_size = 80.f;
   const float max_checker_size = 120.f;
   const float halfperiod = 2000.f; // milliseconds
   const float period = halfperiod + halfperiod;
   const float runtime = state.last_draw_time_ms - state.first_draw_time_ms;

   const float halfcycle_t = fmod((float)runtime, halfperiod) / halfperiod;
   const float mask = fmod((float)runtime, period) <= halfperiod;
   const float rise = halfcycle_t * halfcycle_t * (3.0f - 2.0f * halfcycle_t);
   const float norm_factor = mask * rise + (1.f - mask) * (1.f - rise);

   const uint32_t checker_size = (uint32_t)(min_checker_size + max_checker_size * norm_factor);

   for (uint32_t y = 0; y < height; y++) {
      for (uint32_t x = 0; x < width; x++) {
         Pixel *pixel = (Pixel *)(data + y * stride + x * bytes_per_pixel);
         if ((x + y / checker_size * checker_size) % (checker_size * 2) < checker_size) {
            *pixel = {164, 164, 253}; // light red
         } else {
            *pixel = {164, 253, 164}; // light green
         }

         pixel->a = 0xFF;
      }
   }

   swapchain_buffer_unmap(buffer, map_cookie);
}

static bool
draw_surface_and_commit(Surface *surface) {
   // request the next frame (must be before the related commit)
   struct wl_callback *callback = wl_surface_frame(surface->instance);
   wl_callback_add_listener(callback, &surface_frame_listener, (void *)surface);

   if (!surface->swapchain->ready_buffers) {
      ERROR("no ready buffers during frame update...delaying");
      return false;
   }

   // linked-list pop front
   Swapchain_Buffer *swapchain_buffer = surface->swapchain->ready_buffers;
   surface->swapchain->ready_buffers = swapchain_buffer->next;
   swapchain_buffer->prev = swapchain_buffer->next = NULL;

   draw_to_buffer(swapchain_buffer);
   wl_surface_attach(surface->instance, swapchain_buffer->buffer, 0, 0);
   wl_surface_damage_buffer(surface->instance, 0, 0, INT32_MAX, INT32_MAX);
   wl_surface_commit(surface->instance);

   surface->draw_pending_count -= 1;
   return true;
}

int main(int argc, char **argv) {
   state.display = wl_display_connect(NULL);
   if (!state.display) {
      ERROR("Failed to connect to display");
      return 1;
   }
   DEBUG("Opened display (%p)", state.display);

   state.registry = wl_display_get_registry(state.display);
   wl_registry_add_listener(state.registry, &registry_listener, NULL);
   wl_display_roundtrip(state.display);
   DEBUG("Set up global registry");

   // General Wayland Setup
   bind_compositor();
   bind_shm();
   bind_xdg_shell();
   state.linux_dmabuf = linux_dmabuf_create(LINUX_DMABUF_VERSION_TO_BIND);

#if USE_GBM_BUFFERS
   if (!state.linux_dmabuf) {
      ERROR("zwp_linux_dmabuf_v1 is required for GBM buffer support.");
      return 1;
   }
#endif

   // create and configure a presentation surface
   Surface *surface = create_surface("wayland-tests", 800, 800);

#if STRESS_TEST_COUNT > 0
   INFO("Running stress test %d times", STRESS_TEST_COUNT);
   Linux_Dmabuf *temp[STRESS_TEST_COUNT];
   for (uint32_t version = 1; version <= state.linux_dmabuf_global.version; version++) {
      for (uint32_t i = 0; i < ARRAY_SIZE(temp); i++) {
         Linux_Dmabuf *linux_dmabuf = linux_dmabuf_create(version);
         temp[i] = linux_dmabuf;

         if (version >= ZWP_LINUX_DMABUF_V1_GET_DEFAULT_FEEDBACK_SINCE_VERSION) {
            INFO("Get default dmabuf feedback");
            get_default_dmabuf_feedback(temp[i]);
         }

         if (linux_dmabuf->version >= ZWP_LINUX_DMABUF_V1_GET_SURFACE_FEEDBACK_SINCE_VERSION) {
            get_surface_dmabuf_feedback(linux_dmabuf, surface->instance);
            DEBUG("Get surface dmabuf feedback");
         }

         wl_display_roundtrip(state.display);
      }
      for (uint32_t i = 0; i < ARRAY_SIZE(temp); i++) {
         linux_dmabuf_destroy(temp[i]);
         wl_display_roundtrip(state.display);
      }
   }

   exit(0);
#endif

   wl_display_roundtrip(state.display);

   uint32_t frame_count = 0;
   while (DRAW_FRAME_COUNT == -1 || frame_count < DRAW_FRAME_COUNT) {
      frame_count += 1;

      bool ok = true;
      while (ok && surface->draw_pending_count) {
         ok = draw_surface_and_commit(surface);
      }

      int event_count = wl_display_dispatch(state.display);
      if (event_count < 0) {
         ERROR("Wayland dispatch error: %s", strerror(errno));
         return 1;
      }
   }

   return 0;
}
